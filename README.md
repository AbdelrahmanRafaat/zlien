Zlien Task
======

What was done ?
 - Uploading and validation flow
 - Genearting Report from uploaded file
 
Todo:
 - Adding Repositories and save the projects/customer/report data
 - Complete missing end-points
 - Unit Testing
 - E2E Testing

Coding Standards:
 - PSR4
 - PSR2

Used Libraries:
 - nesbot/carbon for date operations

Task Flow:
 - Starting Point for uploading form /reports/upload/ => ReportsController@getUploadAction

Order Processing Flow:
 - Read uploaded file, validate, parse it to an array
 - Take the parsing result and map it to entities (Lien, Notice)

Please Note:
 - It`s my first time to use Symfony so i`m not very familiar with it`s best practicies.