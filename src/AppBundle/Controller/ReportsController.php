<?php

namespace AppBundle\Controller;

use Helpers\ProjectsCollectionHelper;
use Reports\Entities\Project;
use Reports\Files\Reader;
use Reports\Files\Constants;
use Reports\Files\Validator;
use Reports\Mappers\ProjectFactory;
use Reports\Mappers\ProjectsMapper;
use Reports\Mappers\ProjectValidator;
use Reports\Files\OrdersHandlerFacade;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ReportsController
 *
 * @package AppBundle\Controller
 */
class ReportsController extends Controller
{
    const REPORTS_VIEWS_DIRECTORY = 'reports/';

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUploadAction(): Response
    {
        return $this->render(static::REPORTS_VIEWS_DIRECTORY . 'upload.html.twig', ['error' => '']);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postUploadAction(Request $request): Response
    {
        try {
            $orders   = (new OrdersHandlerFacade($request, new Validator, new Reader))->handle();
            $projects = (new ProjectsMapper($orders, new ProjectValidator($orders[Constants::COLUMNS]), new ProjectFactory))->map();

            $projects = array_map(function (Project $project) {
                $project->getDeadline();

                return $project;
            }, $projects);
            $projects = ProjectsCollectionHelper::removeDuplicates($projects);

            return $this->render(static::REPORTS_VIEWS_DIRECTORY . 'show.html.twig', ['projects' => $projects]);
        } catch (\Exception $exception) {
            $error = $exception->getMessage();

            return $this->render(static::REPORTS_VIEWS_DIRECTORY . 'upload.html.twig', ['error' => $error]);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(): Response
    {
        return $this->render(static::REPORTS_VIEWS_DIRECTORY . 'list.html.twig');
    }

    /**
     * @param int $reportId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(int $reportId): Response
    {
        return $this->render(static::REPORTS_VIEWS_DIRECTORY . 'show.html.twig');
    }

    /**
     * @param string $customerName
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function customerReportsAction(string $customerName): Response
    {
        return $this->render(static::REPORTS_VIEWS_DIRECTORY . 'list_customer_reports.html.twig');
    }
}