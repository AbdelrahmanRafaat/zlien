<?php

namespace Helpers;

use Carbon\Carbon;

/**
 * Class StringHelpers
 *
 * @package Helpers
 */
class DatesHelpers
{
    const REPORT_DATE_FORMAT = 'm-d-Y';
    const DATE_DELIMITER     = '-';

    /**
     * @param string $date
     *
     * @return \Carbon\Carbon
     */
    public static function toCarbonInstance(string $date = ''): Carbon
    {
        if (empty($date)) {
            return Carbon::now()->startOfDay();
        }

        $date = static::unifyDate($date);

        return Carbon::createFromFormat(static::REPORT_DATE_FORMAT, $date)->startOfDay();
    }

    /**
     * @param string $date
     *
     * @return string
     */
    public static function unifyDate(string $date): string
    {
        return str_replace('/', static::DATE_DELIMITER, $date);
    }

    /**
     * @param string $date
     *
     * @return bool
     */
    public static function isValidDate(string $date): bool
    {
        if (empty($date)) {
            return false;
        }
        
        $date       = static::unifyDate($date);
        $datePieces = explode(static::DATE_DELIMITER, $date);
        if (count($datePieces) != 3) {
            return false;
        }

        $day   = (int)$datePieces[1] ?? 0;
        $month = (int)$datePieces[0] ?? 0;
        $year  = (int)$datePieces[2] ?? 0;

        return checkdate($month, $day, $year);
    }

    /**
     * @param \Carbon\Carbon $date
     *
     * @return bool
     */
    public static function isWeekEnd(Carbon $date): bool
    {
        return $date->isSaturday() || $date->isSunday();
    }

    /**
     * @param int $year
     *
     * @return array
     */
    public static function getHolidays(int $year): array
    {
        return [
            static::toCarbonInstance('12-31-' . $year),
            static::toCarbonInstance('06-02-' . $year),
            static::toCarbonInstance('11-24-' . $year),
            Carbon::parse('second friday december ' . $year)->startOfDay(),
        ];
    }

    /**
     * @param \Carbon\Carbon $date
     *
     * @return bool
     */
    public static function isHoliday(Carbon $date): bool
    {
        $holidays = static::getHolidays($date->year);
        foreach ($holidays as $holiday) {
            if ($date->equalTo($holiday)) {
                return true;
            }
        }

        return false;
    }
}