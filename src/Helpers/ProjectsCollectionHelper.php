<?php

namespace Helpers;

/**
 * Class ProjectsCollectionHelper
 *
 * @package Helpers
 */
class ProjectsCollectionHelper
{
    /**
     * @param array $projects
     *
     * @return array
     */
    public static function removeDuplicates(array $projects): array
    {
        $uniqueProjects = [];

        foreach ($projects as $project) {
            $uniqueProjects[$project->getIdentifier()] = $project;
        }

        return$uniqueProjects;
    }
}