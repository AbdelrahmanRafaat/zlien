<?php

namespace Reports\Deadline;

use Carbon\Carbon;
use Helpers\DatesHelpers;
use Reports\Entities\Project;

/**
 * Class Calculator
 *
 * @package Reports\Deadline
 */
abstract class Calculator
{
    /**
     * @param \Reports\Entities\Project $project
     *
     * @return \Carbon\Carbon
     */
    abstract public function calculateDeadLine(Project $project): Carbon;

    /**
     * @param \Carbon\Carbon $date
     *
     * @return \Carbon\Carbon
     */
    protected function getNonHolidayOrWeekEnd(Carbon $date): Carbon
    {
        $isWeekEnd = DatesHelpers::isWeekEnd($date);
        $isHoliday = DatesHelpers::isHoliday($date);

        if(!$isWeekEnd && !$isHoliday){
            return $date;
        }

        return $this->getNonHolidayOrWeekEnd($date->subDay());
    }
}