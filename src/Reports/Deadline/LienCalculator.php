<?php

namespace Reports\Deadline;

use Carbon\Carbon;
use Reports\Entities\Project;

/**
 * Class LienCalculator
 *
 * @package Reports\Deadline
 */
class LienCalculator extends Calculator
{
    /**
     * @param \Reports\Entities\Project $lien
     *
     * @return \Carbon\Carbon
     */
    public function calculateDeadLine(Project $lien): Carbon
    {
        $deadLine =  $lien->getStartDate()->lastOfMonth()->addDays(90);

        return $this->getNonHolidayOrWeekEnd($deadLine);
    }
}