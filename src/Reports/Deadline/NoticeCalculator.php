<?php

namespace Reports\Deadline;

use Carbon\Carbon;
use Reports\Entities\Project;

/**
 * Class NoticeCalculator
 *
 * @package Reports\Deadline
 */
class NoticeCalculator extends Calculator
{
    /**
     * @param \Reports\Entities\Notice $notice
     *
     * @return \Carbon\Carbon
     */
    public function calculateDeadLine(Project $notice): Carbon
    {
        $deadLine = strtolower($notice->getCity()) == 'texas' ? $this->getTexasDeadLine($notice) : $notice->getCommencementDate()->lastOfMonth()->addDays(60);

        return $this->getNonHolidayOrWeekEnd($deadLine);
    }

    /**
     * @param \Reports\Entities\Notice $notice
     *
     * @return \Carbon\Carbon
     */
    private function getTexasDeadLine(Project $notice): Carbon
    {
        return $notice->getCommencementDate()->lastOfMonth()->addDays(15);
    }
}