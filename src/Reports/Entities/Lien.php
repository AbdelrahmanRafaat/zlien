<?php

namespace Reports\Entities;

use Carbon\Carbon;
use Reports\Deadline\LienCalculator;

/**
 * Class Lien
 *
 * @package Entities
 */
class Lien extends Project
{
    /**
     * @return \Carbon\Carbon
     */
    public function getDeadline(): Carbon
    {
        if(isset($this->deadLine)){
            return $this->deadLine;
        }

        $deadLine = (new LienCalculator())->calculateDeadLine($this);
        $this->deadLine = $deadLine;

        return $deadLine;
    }
}