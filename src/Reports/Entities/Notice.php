<?php

namespace Reports\Entities;

use Carbon\Carbon;
use Helpers\DatesHelpers;
use Reports\Deadline\NoticeCalculator;
use Reports\Files\Constants;

/**
 * Class Notice
 *
 * @package Entities
 */
class Notice extends Project
{
    /**@var Carbon */
    private $commencementDate;

    /**
     * Notice constructor.
     *
     * @param array $project
     * @param array $columns
     */
    public function __construct(array $project, array $columns)
    {
        parent::__construct($project, $columns);

        $this->setCommencementDate($project[$columns[Constants::PROJECT_COMMENCEMENT_DATE]]);
    }

    /**
     * @param string $date
     */
    public function setCommencementDate(string $date): void
    {
        if (empty($date) || !DatesHelpers::isValidDate($date)) {
            $this->commencementDate = DatesHelpers::toCarbonInstance();
        }

        $this->commencementDate = DatesHelpers::toCarbonInstance($date);
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCommencementDate(): Carbon
    {
        return $this->commencementDate;
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getDeadline(): Carbon
    {
        if(isset($this->deadLine)){
            return $this->deadLine;
        }

        $deadLine = (new NoticeCalculator)->calculateDeadLine($this);
        $this->deadLine = $deadLine;

        return $deadLine;
    }
}