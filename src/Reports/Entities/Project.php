<?php

namespace Reports\Entities;

use Carbon\Carbon;
use Helpers\DatesHelpers;
use Reports\Files\Constants;

/**
 * Class Project
 *
 * @package Entities
 */
abstract class Project
{
    /** @var string */
    private $customerName;
    /** @var string */
    private $name;
    /** @var string */
    private $address;
    /** @var string */
    private $city;
    /** @var string */
    private $state;
    /** @var int */
    private $zip;
    /** @var \Carbon\Carbon */
    private $startDate;
    /** @var \Carbon\Carbon*/
    protected $deadLine;

    public function __construct(array $project, array $columns)
    {
        $this->setStartDate($project[$columns[Constants::PROJECT_START_DATE]]);
        $this->setCustomerName($project[$columns[Constants::CUSTOMER_NAME]]);
        $this->setAddress($project[$columns[Constants::PROJECT_ADDRESS]]);
        $this->setState($project[$columns[Constants::PROJECT_STATE]]);
        $this->setName($project[$columns[Constants::PROJECT_NAME]]);
        $this->setCity($project[$columns[Constants::PROJECT_CITY]]);
        $this->setZip($project[$columns[Constants::PROJECT_ZIP]]);
    }

    abstract public function getDeadline(): Carbon;

    /**
     * @param string $customerName
     */
    protected function setCustomerName(string $customerName): void
    {
        $this->customerName = $customerName;
    }

    public function getCustomerName(): string
    {
        return $this->customerName;
    }

    /**
     * @param string $name
     */
    protected function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $address
     */
    protected function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $city
     */
    protected function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $state
     */
    protected function setState(string $state): void
    {
        $this->state = $state;
    }

    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param int $zip
     */
    protected function setZip(int $zip): void
    {
        $this->zip = $zip;
    }

    public function getZip(): int
    {
        return $this->zip;
    }

    /**
     * @param string $startDate
     */
    protected function setStartDate(string $startDate): void
    {
        $this->startDate = DatesHelpers::toCarbonInstance(DatesHelpers::unifyDate($startDate));
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return md5($this->getAddress() . $this->getCity() . $this->getState() . $this->getZip());
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getStartDate(): Carbon
    {
        return $this->startDate;
    }
}