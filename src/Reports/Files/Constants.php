<?php

namespace Reports\Files;

/**
 * Class Constants
 *
 * @package Reports\Files
 */
class Constants
{
    const REQUEST_FILE_NAME   = 'orders';
    const CSV_FILE_FORMAT     = 'csv';
    const MAX_FILE_SIZE_IN_KB = '5000';

    /** Exceptions messages */
    const UNREADABLE_FILE_EXCEPTION_MESSAGE           = 'File is not readable';
    const UNSUPPORTED_FILE_EXCEPTION_MESSAGE          = 'Make sure that file format is' . self::CSV_FILE_FORMAT;
    const FILE_EXCEEDED_SIZE_LIMIT_EXCEPTION_MESSAGE  = 'File size should be less than ' . self::MAX_FILE_SIZE_IN_KB . ' KB';
    const MISSING_MANDATORY_COLUMNS_EXCEPTION_MESSAGE = 'File Missing %s columns';
    const EMPTY_PROJECTS_EXCEPTION_MESSAGE            = 'File should have at least one project';
    const NO_FILES_PROVIDED                           = 'No file was uploaded, select a file first';

    const COLUMNS  = 'columns';
    const PROJECTS = 'projects';

    const CUSTOMER_NAME             = 'customer_name';
    const PROJECT_NAME              = 'project_name';
    const PROJECT_ADDRESS           = 'project_address';
    const PROJECT_CITY              = 'project_city';
    const PROJECT_STATE             = 'project_state';
    const PROJECT_ZIP               = 'project_zip';
    const PROJECT_START_DATE        = 'project_start_date';
    const PROJECT_OUTSTANDING_DEBT  = 'project_outstanding_debt';
    const PROJECT_COMMENCEMENT_DATE = 'project_commencement_date';

    const MANDATORY_COLUMNS = [
        self::CUSTOMER_NAME,
        self::PROJECT_NAME,
        self::PROJECT_ADDRESS,
        self::PROJECT_CITY,
        self::PROJECT_STATE,
        self::PROJECT_ZIP,
        self::PROJECT_START_DATE,
    ];
}