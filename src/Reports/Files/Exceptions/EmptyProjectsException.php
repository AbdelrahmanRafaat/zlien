<?php

namespace Reports\Files\Exceptions;

use Reports\Files\Constants;

/**
 * Class EmptyProjectsException
 *
 * @package Reports\Files\Exceptions
 */
class EmptyProjectsException extends \Exception
{
    public function __construct()
    {
        parent::__construct(Constants::EMPTY_PROJECTS_EXCEPTION_MESSAGE);
    }
}