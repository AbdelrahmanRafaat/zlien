<?php

namespace Reports\Files\Exceptions;

use Reports\Files\Constants;

/**
 * Class FileExceededSizeLimitException
 *
 * @package Reports\Files\Exceptions
 */
class FileExceededSizeLimitException extends \Exception
{
    /**
     * FileExceededSizeLimitException constructor.
     */
    public function __construct()
    {
        parent::__construct(Constants::FILE_EXCEEDED_SIZE_LIMIT_EXCEPTION_MESSAGE, 0, null);
    }
}