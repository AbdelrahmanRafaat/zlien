<?php

namespace Reports\Files\Exceptions;

use Reports\Files\Constants;

/**
 * Class MissingMandatoryColumnException
 *
 * @package Reports\Files\Exceptions
 */
class MissingMandatoryColumnsException extends \Exception
{
    public function __construct(array $missingColumns)
    {
        parent::__construct(sprintf(Constants::MISSING_MANDATORY_COLUMNS_EXCEPTION_MESSAGE, join(',', $missingColumns)));
    }
}