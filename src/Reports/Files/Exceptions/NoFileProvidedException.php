<?php

namespace Reports\Files\Exceptions;

use Reports\Files\Constants;
use Throwable;

/**
 * Class NoFileProvidedException
 *
 * @package Reports\Files\Exceptions
 */
class NoFileProvidedException extends \Exception
{
    public function __construct()
    {
        parent::__construct(Constants::NO_FILES_PROVIDED);
    }
}