<?php

namespace Reports\Files\Exceptions;

use Reports\Files\Constants;

/**
 * Class NotReadableFileException
 *
 * @package Reports\Files\Exceptions
 */
class UnReadableFileException extends \Exception
{
    /**
     * UnReadableFileException constructor.
     */
    public function __construct()
    {
        parent::__construct(Constants::UNREADABLE_FILE_EXCEPTION_MESSAGE, 0, null);
    }
}