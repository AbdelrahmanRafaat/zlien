<?php

namespace Reports\Files\Exceptions;

use Reports\Files\Constants;

/**
 * Class UnSupportedFileException
 *
 * @package Reports\Files\Exceptions
 */
class UnSupportedFileException extends \Exception
{
    /**
     * UnReadableFileException constructor.
     */
    public function __construct()
    {
        parent::__construct(Constants::UNSUPPORTED_FILE_EXCEPTION_MESSAGE, 0, null);
    }
}