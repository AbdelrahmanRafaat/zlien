<?php

namespace Reports\Files;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class UploadedFileHandlerFacade
 *
 * @package Reports\Files
 */
class OrdersHandlerFacade
{
    /** @var \Symfony\Component\HttpFoundation\Request */
    private $request;
    /** @var \Reports\Files\Validator */
    private $validator;
    /** @var \Reports\Files\Reader */
    private $reader;

    /**
     * OrdersHandlerFacade constructor.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Reports\Files\Validator                  $validator
     * @param \Reports\Files\Reader                     $reader
     */
    public function __construct(Request $request, Validator $validator, Reader $reader)
    {
        $this->request   = $request;
        $this->validator = $validator;
        $this->reader    = $reader;
    }

    /**
     * @return array
     *
     * @throws \Reports\Files\Exceptions\EmptyProjectsException
     * @throws \Reports\Files\Exceptions\FileExceededSizeLimitException
     * @throws \Reports\Files\Exceptions\MissingMandatoryColumnsException
     * @throws \Reports\Files\Exceptions\UnReadableFileException
     * @throws \Reports\Files\Exceptions\UnSupportedFileException
     */
    public function handle(): array
    {
        $this->validator->validateRequestFile($this->request);
        $orders = $this->reader->read($this->request);
        $this->validator->validateOrders($orders);

        return $orders;
    }

}