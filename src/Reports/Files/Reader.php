<?php

namespace Reports\Files;

use Symfony\Component\HttpFoundation\Request;
use \SplFileObject as File;


/**
 * Class Reader
 *
 * @package Reports\Files
 */
class Reader
{
    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return array
     */
    public function read(Request $request): array
    {
        return $this->parse($this->open($request));
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return File
     */
    private function open(Request $request): File
    {
        return $request->files->get(Constants::REQUEST_FILE_NAME)->openFile();
    }

    /**
     * @param File $ordersFile
     *
     * @return array
     */
    private function parse(File $ordersFile): array
    {
        $ordersArray = [];

        while ($data = $ordersFile->fgetcsv()) {
            if (empty($ordersArray[Constants::COLUMNS])) {
                $ordersArray[Constants::COLUMNS] = array_map(function ($columnName) {
                    return strtolower(trim($columnName));
                }, $data);
                continue;
            }

            $ordersArray[Constants::PROJECTS][] = $data;
        }

        return $ordersArray;
    }
}