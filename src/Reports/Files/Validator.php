<?php

namespace Reports\Files;

use Reports\Files\Exceptions\EmptyProjectsException;
use Reports\Files\Exceptions\FileExceededSizeLimitException;
use Reports\Files\Exceptions\MissingMandatoryColumnsException;
use Reports\Files\Exceptions\NoFileProvidedException;
use Reports\Files\Exceptions\UnReadableFileException;
use Reports\Files\Exceptions\UnSupportedFileException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Validator
 *
 * @package Reports\Files
 */
class Validator
{
    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @throws \Reports\Files\Exceptions\UnReadableFileException
     * @throws \Reports\Files\Exceptions\UnSupportedFileException
     * @throws \Reports\Files\Exceptions\FileExceededSizeLimitException
     * @throws \Reports\Files\Exceptions\NoFileProvidedException
     */
    public function validateRequestFile(Request $request): void
    {
        $orders = $request->files->get(Constants::REQUEST_FILE_NAME);

        if(empty($orders)){
            throw new NoFileProvidedException;
        }

        if (!$orders->isReadable()) {
            throw new UnReadableFileException;
        }

        if ($orders->getClientOriginalExtension() != Constants::CSV_FILE_FORMAT || !$orders->isValid()) {
            throw new UnSupportedFileException;
        }

        if ($orders->getSize() > Constants::MAX_FILE_SIZE_IN_KB) {
            throw new FileExceededSizeLimitException;
        }
    }

    /**
     * @param array $orders
     *
     * @throws \Reports\Files\Exceptions\MissingMandatoryColumnsException
     * @throws \Reports\Files\Exceptions\EmptyProjectsException
     */
    public function validateOrders(array $orders): void
    {
        $this->validateMandatoryColumns($orders[Constants::COLUMNS] ?? []);
        $this->validateProjectsNotEmpty($orders[Constants::PROJECTS] ?? []);
    }

    /**
     * @param array $columns
     *
     * @throws \Reports\Files\Exceptions\MissingMandatoryColumnsException
     */
    private function validateMandatoryColumns(array $columns): void
    {
        $mandatoryColumns = Constants::MANDATORY_COLUMNS;
        $missingColumns   = [];
        foreach ($mandatoryColumns as $mandatoryColumn) {
            if (!in_array($mandatoryColumn, $columns)) {
                $missingColumns[] = $mandatoryColumn;
            }
        }

        if (count($missingColumns) > 0) {
            throw new MissingMandatoryColumnsException($missingColumns);
        }
    }

    /**
     * @param array $projects
     *
     * @throws \Reports\Files\Exceptions\EmptyProjectsException
     */
    private function validateProjectsNotEmpty(array $projects): void
    {
        if (empty($projects)) {
            throw new EmptyProjectsException;
        }
    }
}