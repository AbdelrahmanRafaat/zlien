<?php

namespace Reports\Mappers;

/**
 * Class Constants
 *
 * @package Reports\Mappers
 */
class Constants
{
    const INVALID_PROJECT_DATA = 'Project # %d has invalid data %s';
}