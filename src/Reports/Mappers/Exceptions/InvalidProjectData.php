<?php

namespace Reports\Mappers\Exceptions;

use Reports\Mappers\Constants;
use Throwable;

/**
 * Class InvalidProjectData
 *
 * @package Reports\Mappers\Exceptions
 */
class InvalidProjectData extends \Exception
{
    /**
     * InvalidProjectData constructor.
     *
     * @param int    $projectIndex
     * @param string $invalidColumn
     */
    public function __construct(int $projectIndex, string $invalidColumn)
    {
        parent::__construct(
            sprintf(Constants::INVALID_PROJECT_DATA, $projectIndex + 1, $invalidColumn),
            0, null
        );
    }
}