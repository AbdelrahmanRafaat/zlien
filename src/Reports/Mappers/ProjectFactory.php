<?php

namespace Reports\Mappers;

use Reports\Entities\Lien;
use Reports\Entities\Notice;
use Reports\Entities\Project;
use Reports\Files\Constants;

/**
 * Class ProjectFactory
 *
 * @package Reports\Mappers
 */
class ProjectFactory
{
    /**
     * @param array $project
     * @param array $columns
     *
     * @return \Reports\Entities\Project
     */
    public function make(array $project, array $columns): Project
    {
        if (!empty($project[$columns[Constants::PROJECT_COMMENCEMENT_DATE]])) {
            return new Notice($project, $columns);
        }

        return new Lien($project, $columns);
    }
}