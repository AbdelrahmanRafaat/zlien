<?php

namespace Reports\Mappers;

use Helpers\DatesHelpers;
use Reports\Files\Constants;

/**
 * Class ProjectValidator
 *
 * @package Reports\Mappers
 */
class ProjectValidator
{
    /** @var array */
    private $columns;

    /**
     * ProjectValidator constructor.
     *
     * @param array $columns
     */
    public function __construct(array $columns)
    {
        $this->columns = array_flip($columns);
    }

    /**
     * @param array $project
     *
     * @return null|string
     */
    public function validateProject(array $project): ?string
    {
        $emptyColumn = $this->validateNotEmpty($project);
        if ($emptyColumn != null) {
            return $emptyColumn;
        }

        if (!$this->isValidProjectZip($project)) {
            return Constants::PROJECT_ZIP;
        }

        if (!$this->isValidStartDate($project)) {
            return Constants::PROJECT_START_DATE;
        }

        return null;
    }

    /**
     * @param array $project
     *
     * @return null|string
     */
    private function validateNotEmpty(array $project): ?string
    {
        $shouldNotBeEmpty = [
            Constants::CUSTOMER_NAME,
            Constants::PROJECT_NAME,
            Constants::PROJECT_STATE,
            Constants::PROJECT_ADDRESS,
            Constants::PROJECT_CITY,
            Constants::PROJECT_STATE,
            Constants::PROJECT_ZIP,
            Constants::PROJECT_START_DATE,
        ];

        foreach ($shouldNotBeEmpty as $column) {
            if (empty($project[$this->columns[$column]])) {
                return $column;
            }
        }

        return null;
    }

    /**
     * @param array $project
     *
     * @return bool
     */
    private function isValidProjectZip(array $project): bool
    {
        $zip = (int)($project[$this->columns[Constants::PROJECT_ZIP]]);

        return is_numeric($zip) && strlen($zip) == 5;
    }

    /**
     * @param array $project
     *
     * @return bool
     */
    private function isValidStartDate(array $project): bool
    {
        $startDate = $project[$this->columns[Constants::PROJECT_START_DATE]];

        return DatesHelpers::isValidDate(DatesHelpers::unifyDate($startDate));
    }
}