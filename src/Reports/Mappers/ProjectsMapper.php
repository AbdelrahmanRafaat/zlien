<?php

namespace Reports\Mappers;

use Reports\Files\Constants;
use Reports\Mappers\Exceptions\InvalidProjectData;

/**
 * Class ProjectsMapper
 *
 * @package Reports\Mappers
 */
class ProjectsMapper
{
    /** @var array */
    private $columns;
    /** @var array */
    private $projects;

    /** @var \Reports\Mappers\ProjectValidator */
    private $projectValidator;
    /** @var \Reports\Mappers\ProjectFactory */
    private $projectFactory;

    /**
     * ProjectsMapper constructor.
     *
     * @param array                             $orders
     * @param \Reports\Mappers\ProjectValidator $projectValidator
     * @param \Reports\Mappers\ProjectFactory   $projectFactory
     */
    public function __construct(array $orders, ProjectValidator $projectValidator, ProjectFactory $projectFactory)
    {
        $this->columns  = array_flip($orders[Constants::COLUMNS]);
        $this->projects = $orders[Constants::PROJECTS];

        $this->projectValidator = $projectValidator;
        $this->projectFactory   = $projectFactory;
    }

    /**
     * @return array
     * @throws \Reports\Mappers\Exceptions\InvalidProjectData
     */
    public function map(): array
    {
        $entities = [];

        foreach ($this->projects as $index => $project) {
            $invalidColumn = $this->projectValidator->validateProject($project);

            if ($invalidColumn != null) {
                throw new InvalidProjectData($index, $invalidColumn);
            }

            $entities[] = $this->projectFactory->make($project, $this->columns);
        }

        return $entities;
    }


}